# S-PWM for LabVIEW Hobbyist

A software-based implementation of PWM, similar in functionality to that of wiring pi's, but written in native G code. 
This relies on LabVIEW Hobbyist (LINX) to control the underlying IO.

PWM Frequency is hardcoded to 100 Hz, and Duty Cycle is specified as an integer between 0 and 100 (corresponding to the percentage).

For usage, see "Example.vi" in the `tests` folder. For reference, however, the block diagram appears as thus:

![Screenshot of Example VI](doc/Example-Screenshot.png)

So far I've tried testing up to 6 channels with minimal processor loading and the output waveforms seem to stay reasonable when looking at them
with a scope. Right now I'm considering this beta quality until I can better quantify that. Any help someone can offer with scalability
testing would be appreciated.

Code is saved in LabVIEW 2020 and covered by the MIT License.
